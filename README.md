## future-mall 逆向代码生成器 
```text
项目采用了人人开源的逆向工程生成器，再次感谢人人开源对快速开发平台做出的贡献。
```
- renren-generator是人人开源项目的代码生成器，可在线生成entity、xml、dao、service、html、js、sql代码，减少70%以上的开发任务


## SQL资源
![](https://gavinpic.oss-cn-beijing.aliyuncs.com/picgo20200902192457.png)
MySQL数据库
![](https://gavinpic.oss-cn-beijing.aliyuncs.com/picgo20200902192554.png)

里面包含了建库语句，idea连上数据库后可直接执行。
